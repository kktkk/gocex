HIGHT_NORMAIZER = 300  # ??? why 300???
START_X = 0.0
START_Y = 0.0
START_Z = 0.0
START_A = 180
GOAL_X = 0.0
GOAL_Y = 0.25
GOAL_Z = ''
GOAL_A = 180

MIN_2D_AREA = -30
MAX_2D_AREA = 30

max_obst_anglZ = 15.0 # deg
reduced_map = False
reduced_map_factor = 100
reduced_map_show = False
show_obstacles = False
show_map = False
show_animation = False
path_selection = 'rrt'