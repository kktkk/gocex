from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
from config import *


def defObstacle(data):
  print 'Define Obstacle'
  a, b, c = [], [], []
  delta = np.tan(max_obst_anglZ)
  for i in range(len(data[2])):
    if i < max(range(len(data[2]))):
      if data[2][i] > data[2][i+1] * delta:
        a.append(data[0][i])
        b.append(data[1][i])
        c.append(data[2][i])
  return [a, b, c]


def genObstacles2D(data):
  print 'Generate Obstacles for 2D'
  obstacles = []
  for i in range(len(data[0])):
    obstacles.append((
    data[0][i], 
    data[1][i], 
    (np.sqrt(data[0][i] ** 2 + data[1][i] ** 2)) / 2
  ))
  return obstacles


def showObstacles(data):
  print 'Plotting obstacles with size in 2D Map'
  # set size 
  plt.plot(START_X, START_Y, '^g')
  plt.plot(GOAL_X, GOAL_Y, '^r')
  for i in range(len(data[0])):
    plt.plot(
      data[0][i],
      data[1][i], 
      'ob', 
      ms=np.sqrt(data[0][i] ** 2 + data[1][i] ** 2)
    )
