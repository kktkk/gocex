import os
import subprocess


def getData(x, y, va, z):
  print 'Getting dataset from laser.'
  cmd = ['docker', 'exec', 'env-emu-dev_application_1', \
    './console', 'app:sensor:laser', '-v', str(x), str(y), str(va), str(z)
  ]
  lines = subprocess.check_output(cmd)
  data = lines.split('\n')[0:-3]
  return data


def getFullMapData():
  print 'Get Full Map for debug'
  ang = [0, 30, 60, 90, 120, 150, 180]
  for x in ang:
    for y in ang:
      for va in ang:
        for z in ang:
          data = getData(x, y, va, z)
  return data


def sortXYZ(x, y, va, z):
  print 'Sorting dataset to x, y, z list/arrays'
  data = getData(x, y, va, z)
  a, b, c = [], [], []
  for line in data:
    a.append((
      float(line.split(' ')[0].split(':')[1])
    ))
    b.append((
      float(line.split(' ')[1].split(':')[1])
    ))
    c.append((
      float(line.split(' ')[2].split(':')[1])
    ))
  return [a, b, c]


def sortXYZFullMap():
  print 'Sorting dataset to x, y, z list/arrays for Full Map'
  data = getFullMapData()
  a, b, c = [], [], []
  for line in data:
    a.append((
      float(line.split(' ')[0].split(':')[1])
    ))
    b.append((
      float(line.split(' ')[1].split(':')[1])
    ))
    c.append((
      float(line.split(' ')[2].split(':')[1])
    ))
  return [a, b, c]