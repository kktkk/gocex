from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np


def plotMap(data):
  print 'Plotting 3D map'
  x, y, z = data[0], data[1], data[2]
  fig = plt.figure()
  ax = Axes3D(fig)
  #ax.scatter([0], [0], [300], '^b')
  ax.set_xlabel('x')
  ax.set_ylabel('y')
  ax.set_zlabel('z')
  ax.plot_trisurf(x, y, z, color='blue', shade=True)
            

def plotStartEnd(endx, endy, endz):
  print 'Plotting start and end in 3D Map'
  fig = plt.figure()
  ax = plt.axes(projection='3d')
  ax.scatter([0], [0], [0], 'og')
  ax.scatter([endx], [endy], [endz], '^r')
  ax.set_xlabel('x')
  ax.set_ylabel('y')
  ax.set_zlabel('z')
  ax.plot_trisurf(x, y, z)