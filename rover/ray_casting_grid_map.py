"""

Ray casting 2D grid map example

author: Atsushi Sakai (@Atsushi_twi)

"""

import math
import numpy as np
import matplotlib.pyplot as plt


def calc_grid_map_config(ox, oy, xyreso, scanArea):
    minx = round(min(ox) - scanArea / 2.0)
    miny = round(min(oy) - scanArea / 2.0)
    maxx = round(max(ox) + scanArea / 2.0)
    maxy = round(max(oy) + scanArea / 2.0)
    xw = int(round((maxx - minx) / xyreso))
    yw = int(round((maxy - miny) / xyreso))

    return minx, miny, maxx, maxy, xw, yw


class precastDB:

    def __init__(self):
        self.px = 0.0
        self.py = 0.0
        self.d = 0.0
        self.angle = 0.0
        self.ix = 0
        self.iy = 0

    def __str__(self):
        return str(self.px) + "," + str(self.py) + "," + str(self.d) + "," + str(self.angle)


def atan_zero_to_twopi(y, x):
    angle = math.atan2(y, x)
    if angle < 0.0:
        angle += math.pi * 2.0

    return angle


def precasting(minx, miny, xw, yw, xyreso, yawreso):

    precast = [[] for i in range(int(round((math.pi * 2.0) / yawreso)) + 1)]

    for ix in range(xw):
        for iy in range(yw):
            px = ix * xyreso + minx
            py = iy * xyreso + miny

            d = math.sqrt(px**2 + py**2)
            angle = atan_zero_to_twopi(py, px)
            angleid = int(math.floor(angle / yawreso))

            pc = precastDB()

            pc.px = px
            pc.py = py
            pc.d = d
            pc.ix = ix
            pc.iy = iy
            pc.angle = angle

            precast[angleid].append(pc)

    return precast


def generate_ray_casting_grid_map(ox, oy, xyreso, shadow, scanArea, rover_width):

    minx, miny, maxx, maxy, xw, yw = calc_grid_map_config(ox, oy, xyreso, scanArea)

    pmap = [[0.0 for i in range(yw)] for i in range(xw)]
    precast = precasting(minx, miny, xw, yw, xyreso, shadow)

    obsList = []
    size = 0

    for (x, y) in zip(ox, oy):

        d = math.sqrt(x**2 + y**2)
        angle = atan_zero_to_twopi(y, x)
        angleid = int(math.floor(angle / shadow))

        gridlist = precast[angleid]

        ix = int(round((x - minx) / xyreso))
        iy = int(round((y - miny) / xyreso))


        for grid in gridlist:
            if grid.d > d:
                pmap[grid.ix][grid.iy] = 0.5
                size = np.tan(shadow) * d + rover_width

        obsList.append([x, y, size])
        pmap[ix][iy] = 1.0
    return pmap, minx, maxx, miny, maxy, xyreso, obsList

def draw_heatmap(data, minx, maxx, miny, maxy, xyreso):
    x, y = np.mgrid[slice(minx - xyreso / 2.0, maxx + xyreso / 2.0, xyreso),
                    slice(miny - xyreso / 2.0, maxy + xyreso / 2.0, xyreso)]
    plt.pcolor(x, y, data, vmax=1.0, cmap=plt.cm.Blues)
    plt.axis("equal")


def main(shadow, obstacles, resolution, scanArea, animation, rover_width):
    print(__file__ + " start!!")

    xyreso = resolution  # x-y grid resolution [m]
    yawreso = np.deg2rad(shadow)

    oxl = []
    oyl = []

    for i in obstacles:
        oxl.append(i[0])
        oyl.append(i[1])
        
    ox = np.asarray(oxl)
    oy = np.asarray(oyl)

    pmap, minx, maxx, miny, maxy, xyreso, obsList = generate_ray_casting_grid_map(
        ox, oy, xyreso, shadow, scanArea, rover_width)

    show_animation = animation
    
    if show_animation:  # pragma: no cover
        plt.cla()
        # draw_heatmap(pmap, minx, maxx, miny, maxy, xyreso)
        for i in obsList:
            plt.plot(i[0], i[1], "ok", ms=1 * i[2])
        plt.plot(0.0, 0.0, "ob")
        plt.pause(3.0)
        #plt.show()
    return obsList


