from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from simple_rrt import main as rrt
from rrt_star_reeds_shep import main as rrt_star
from laser import *
from obstacles import *
from config import *
from map_gen import *

'''
1. set start point: x=0, y=0
2. set start altitude: z=get_altitude
3. scan area
4. return area scanned
5. set goal point: x, y, z
6. select best path to goil
7. return best_path
'''


def reduceMap(data, factor):
  print 'Reducing Map.'
  a, b, c = [], [], []
  for n in range(len(data[2])):
    for i in data[0]:
      if -1 < i < 1:
        a.append(np.divide(data[0][n], factor))
        b.append(np.divide(data[1][n], factor))
        c.append(np.subtract(data[2][n], 300))
        for j in b:
          if not -1 < j < 1:
            a.pop(n)
            b.pop(n)
            c.pop(n)
  return [a, b, c]


def showAsDataFrame(data):
  print 'Show data as pandas dataframe'
  a, b, c = np.asarray(data[0]), \
    np.asarray(data[1]), np.asarray(np.subtract(data[2], [300]))
  df = pd.DataFrame(data=zip(a, b, c), columns=['A', 'B', 'C'])
  return df


def run():
  min2D = MIN_2D_AREA
  max2D = MAX_2D_AREA
  goalY = GOAL_Y

  dt = sortXYZ(START_X, START_Y, START_A, START_Z)
  
  if show_map:
    plotMap(dt)
    plt.show()

  if reduced_map:
    min2D= -(30.0 / reduced_map_factor)
    max2D = (30.0 / reduced_map_factor)
    dt = reduceMap(dt, reduced_map_factor)
    
    if reduced_map_show:
      plotMap(dt)
      plt.show()

  obstacles_list = defObstacle(dt)
  
  if show_obstacles:
    showObstacles(obstacles_list)
    plt.show()

  if path_selection == 'rrt':
    try:
      path = rrt(
        [START_X, START_Y], 
        [GOAL_X, GOAL_Y], 
        genObstacles2D(obstacles_list), 
        [min2D, max2D]
      )
    except:
      if reduced_map:
        goalY -= 1 / reduced_map_factor
        path = rrt(
          [START_X, START_Y], 
          [GOAL_X, goalY], 
          genObstacles2D(obstacles_list), 
          [min2D, max2D]
        )
      else:
        goalY -= 1
        path = rrt(
          [START_X, START_Y], 
          [GOAL_X, goalY], 
          genObstacles2D(obstacles_list), 
          [min2D, max2D]
        )
    print path
    return path

  if path_selection == 'rrt_star':
    try:
      path = rrt_star(
        genObstacles2D(obstacles_list),
        [START_X, START_Y, START_A],
        [GOAL_X, GOAL_Y, GOAL_A],
        [min2D, max2D]
      )
    except:
      if reduced_map:
        goalY -= 1 / reduced_map_factor
        path = rrt_star(
          genObstacles2D(obstacles_list),
          [START_X, START_Y, START_A],
          [GOAL_X, goalY, GOAL_A],
          [min2D, max2D]
        )
      else:
        goalY -= 1
        path = rrt_star(
          genObstacles2D(obstacles_list),
          [START_X, START_Y, START_A],
          [GOAL_X, goalY, GOAL_A],
          [min2D, max2D]
        )

    print path
    return path


if __name__ == "__main__":
  '''
  fullMap = sortXYZFullMap()
  plotMap(fullMap)
  plt.show()
  exit()
  '''

  paths = []
  for i in range(10):
    paths.append(
      run()
    )
  for dataset in paths:
    try:
      if len(dataset[0]) > len(dataset[1]):
        paths.pop[0]
      else:
        paths.pop[1]
    except:
      paths[-1]
  
